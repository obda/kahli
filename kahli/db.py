"""Database integration via SQLAlchemy.

This module instantiates a `Flask-SQLAlchemy`_ instance and a
`Flask-Migrate`_ instance, both of which must later be initialized with
a Flask application instance:

.. code-block:: python

   db.init_app(app)
   migrate.init_app(app)

.. _Flask-SQLAlchemy: http://flask-sqlalchemy.pocoo.org/
.. _Flask-Migrate: https://flask-migrate.readthedocs.io/
"""

import datetime
from pathlib import Path
from typing import Any

from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import DateTime, TypeDecorator

migrations_dir = Path(__file__).parent / "migrations"

db = SQLAlchemy()
migrate = Migrate(db=db, directory=str(migrations_dir))


class TZDateTime(TypeDecorator):
    """A timezone-aware timestamp stored as naive UTC in the database.

    See https://docs.sqlalchemy.org/en/20/core/custom_types.html#store-timezone-aware-timestamps-as-timezone-naive-utc
    """

    impl = DateTime
    cache_ok = True

    def process_bind_param(
        self,
        value: datetime.datetime | None,
        _dialect: Any,  # noqa: ANN401
    ) -> datetime.datetime | None:
        """Convert a timezone-aware datetime into a naive UTC timestamp."""
        if value is None:
            return value
        if not value.tzinfo:
            error_message = "missing tzinfo"
            raise TypeError(error_message)
        return value.astimezone(datetime.UTC).replace(tzinfo=None)

    def process_result_value(
        self,
        value: datetime.datetime | None,
        _dialect: Any,  # noqa: ANN401
    ) -> datetime.datetime | None:
        """Convert a naive UTC timestamp into a timezone-aware one."""
        if value is None:
            return value
        return value.replace(tzinfo=datetime.UTC)
