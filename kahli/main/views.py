"""Views for the ``main`` blueprint."""

from http import HTTPStatus

from flask import Response, abort, current_app, redirect

from ..db import db
from .blueprint import main
from .models import ShortURL


@main.route("/")
def index() -> Response:
    """Show a plain text welcome message."""
    message = "This is kah·li!\n"
    return Response(message, mimetype="text/plain")


@main.route("/<path:key>")
def follow_short_url(key: str) -> Response:
    """Follow a short URL."""
    query = db.select(ShortURL).filter_by(key=key)
    short_url: ShortURL = db.one_or_404(query)
    if short_url.is_expired():
        abort(HTTPStatus.NOT_FOUND)
    return redirect(short_url.target, HTTPStatus.PERMANENT_REDIRECT)


@main.route("/.well-known/healthcheck")
def healthcheck() -> dict:
    """Return status information (as JSON), usable for health checks."""
    version = current_app.config["SENTRY_RELEASE"]
    return {"status": "OK", "version": version}
