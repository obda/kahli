"""CLI commands for the ``main`` blueprint."""

import click

from .blueprint import main


@main.cli.command()
@click.argument("name", default="World")
def welcome(name: str) -> None:
    """Print a “Welcome” message."""
    message = f"Welcome to kah·li, {name}!"
    click.echo(message)
