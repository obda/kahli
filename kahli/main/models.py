"""Models for the ``main`` blueprint."""

import datetime

from ..db import TZDateTime, db


class ShortURL(db.Model):
    """A short URL that is an alias for another URL."""

    short_url_id = db.Column("id", db.Integer(), primary_key=True)
    user_id = db.Column(db.ForeignKey("user.id"))
    user = db.relationship("User", back_populates="short_urls")
    key = db.Column(db.String(31), nullable=False, unique=True)
    target = db.Column(db.Text(), nullable=False)
    expires = db.Column(TZDateTime(), default=None)

    def __repr__(self) -> str:
        """Return the representation string for this instance."""
        return f"<{self.__class__.__name__} {self.key!r} -> {self.target!r}>"

    def is_expired(self) -> bool:
        """Return whether this Short URL is already expired."""
        if not self.expires:
            return False
        utcnow = datetime.datetime.now(tz=datetime.UTC)
        return utcnow > self.expires
