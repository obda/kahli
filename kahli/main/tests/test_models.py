"""Tests for the ``main`` blueprint’s models."""

import datetime

import pytest

from ...db import db
from ...security.models import User
from ...tests.base import TestCaseBase
from ..models import ShortURL


class ShortURLTest(TestCaseBase):
    """Tests for the :class:`ShortURL` model."""

    def test_short_url_user_relationship(self) -> None:
        """Test the relationship to the :class:`User` class."""
        email = "clemens@kaposi.name"
        user = User(
            email=email,
            short_urls=[
                url1 := ShortURL(key="foo", target="https://obda.net"),
                url2 := ShortURL(key="bar", target="https://49ersFanZone.net"),
            ],
            active=True,
            fs_uniquifier="202bb3fb2ea54735b25c9d7e41cbda25",
        )
        db.session.add_all([user, url1, url2])
        db.session.commit()

        query = db.select(User).filter_by(email=email)
        fetched_user = db.session.execute(query).scalar_one()
        assert set(fetched_user.short_urls) == {url1, url2}

    def test_no_expiration(self) -> None:
        """Verify the behavior if no expiration is set."""
        url = ShortURL(key="foo", target="https://obda.net")
        assert url.expires is None
        assert url.is_expired() is False

    @pytest.mark.freeze_time("2022-12-24 00:00:00")
    def test_future_expiration(self) -> None:
        """Verify the behavior for a future expiration date."""
        expires = datetime.datetime(2023, 1, 1, tzinfo=datetime.UTC)
        url = ShortURL(key="foo", target="https://obda.net", expires=expires)
        assert url.is_expired() is False

    @pytest.mark.freeze_time("2022-12-24 00:00:00")
    def test_past_expiration(self) -> None:
        """Verify the behavior for a past expiration date."""
        expires = datetime.datetime(2022, 1, 1, tzinfo=datetime.UTC)
        url = ShortURL(key="foo", target="https://obda.net", expires=expires)
        assert url.is_expired() is True
