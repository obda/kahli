"""Tests for the ``main`` blueprint’s views."""

import datetime
from http import HTTPStatus
from typing import Any

import pytest
from flask import Flask

from ...db import db
from ...tests.base import TestCaseBase
from ..models import ShortURL


class TestIndexView(TestCaseBase):
    """Tests for the index view."""

    def test_index_welcome_message(self) -> None:
        """Check that the index view returns a plain text welcome message."""
        response = self.client.get("/")
        message = response.get_data().decode()
        assert response.mimetype == "text/plain"
        assert "kah·li" in message


class TestFollowShortURL(TestCaseBase):
    """Tests for the ``follow_short_url`` view."""

    def test_known_url_returns_permanent_redirect(self) -> None:
        """Test that following an short URL returns a permanent redirect."""
        key = "fanzone"
        target = "https://49ersfanzone.net"
        db.session.add(ShortURL(key=key, target=target))
        db.session.commit()
        response = self.client.get(f"/{key}")
        assert response.status_code == HTTPStatus.PERMANENT_REDIRECT
        assert response.location == target

    def test_unknown_url_returns_404(self) -> None:
        """Test that following an unknown short URL returns an HTTP 404."""
        response = self.client.get("/i-dont-exist")
        assert response.status_code == HTTPStatus.NOT_FOUND

    @pytest.mark.freeze_time("2022-12-24 00:00:00")
    def test_expired_url_returns_404(self) -> None:
        """Verify that expired URLs returns an HTTP 410 “Gone”."""
        key = "foo"
        expires = datetime.datetime(2022, 1, 1, tzinfo=datetime.UTC)
        url = ShortURL(key=key, target="https://obda.net", expires=expires)
        db.session.add(url)
        db.session.commit()
        response = self.client.get(f"/{key}")
        assert response.status_code == HTTPStatus.NOT_FOUND


class TestHealthcheck(TestCaseBase):
    """Tests for the healthcheck view."""

    def get_default_response(
        self,
        **kwargs: Any,  # noqa: ANN401
    ) -> Flask.response_class:
        """Call the view without parameters, and return the response."""
        return self.client.get("/.well-known/healthcheck", **kwargs)

    def test_json_content_type(self) -> None:
        """Verify that the response uses the JSON content type."""
        response = self.get_default_response()
        assert response.mimetype == "application/json"

    def test_json_content_type_accept_xhtml(self) -> None:
        """Verify that JSON is returned even when not accepted explicitly."""
        response = self.get_default_response(
            headers=[("accept", "text/html,application/xhtml+xml")],
        )
        assert response.mimetype == "application/json"

    def test_http_status_code_ok(self) -> None:
        """Check that the response is served with HTTP status “OK”."""
        response = self.get_default_response()
        assert response.status_code == HTTPStatus.OK

    def test_response_contains_status(self) -> None:
        """Verify that the response contains status information."""
        response = self.get_default_response()
        data = response.get_json()
        assert data["status"] == "OK"
