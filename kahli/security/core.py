"""Core objects for ``Flask-Security-Too integration."""

from flask_security import Security, SQLAlchemyUserDatastore

from ..db import db
from .models import Role, User, WebAuthn

user_datastore = SQLAlchemyUserDatastore(db, User, Role, WebAuthn)
security = Security(datastore=user_datastore)
