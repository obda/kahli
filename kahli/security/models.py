"""Security data models."""

from flask_security.models.fsqla_v3 import (
    FsModels,
    FsRoleMixin,
    FsUserMixin,
    FsWebAuthnMixin,
)

from ..db import db

FsModels.set_db_info(db)


class User(db.Model, FsUserMixin):
    """An application user."""

    short_urls = db.relationship("ShortURL", back_populates="user")


class Role(db.Model, FsRoleMixin):
    """A collection of permissions."""


class WebAuthn(db.Model, FsWebAuthnMixin):
    """A dummy class."""
