"""kah·li default settings."""

import importlib.metadata
import os

# Customizable configuration
# ==========================
#
# Consider all settings in this section to be “public”, i.e., consider
# potentially any of them to be configured differently in concrete
# application instances.


# Application settings
# --------------------
#: All characters that may appear in a short URL.
KAHLI_ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
#: The number of characters for generated short URLs.
KAHLI_KEY_LENGTH = 5
# noinspection HttpUrlsUsage
#: The list of supported URI schemes.  If the entered target URI does
#: not start with any of these strings, ``KAHLI_DEFAULT_URI_SCHEME``
#: gets prepended to the target URI.
KAHLI_URI_SCHEMES = ("http://", "https://", "ftp://")
#: The default URI scheme.  A target URI gets prefixed with this string
#: if it does not start with any of the strings defined in
#: ``KAHLI_URI_SCHEMES``.
KAHLI_DEFAULT_URI_SCHEME = "https://"

# Configure logging
KAHLI_LOGGING = {
    "version": 1,
    "formatters": {
        "default": {
            "format": "%(asctime)s [%(levelname)s] %(name)s: %(message)s",
        },
        "json": {
            "class": "kahli.logs.CustomJsonFormatter",
            "format": (
                "%(datetime)s %(level)s %(logger)s %(message)s "
                "%(source)s %(lineno)s %(function)s"
            ),
        },
    },
    "handlers": {
        "stream": {
            "class": "logging.StreamHandler",
            "level": "DEBUG",
            "formatter": "default",  # think about using `json` for production
            "stream": "ext://sys.stdout",
        },
    },
    "loggers": {
        "kahli": {
            "level": "DEBUG",
            "propagate": False,
            "handlers": ["stream"],
        },
        # # Enable SQL statement logging
        # "sqlalchemy.engine": {"level": "INFO"},
    },
    "root": {"level": "INFO", "handlers": ["stream"]},
}

# Flask settings
# --------------
SECRET_KEY = os.environ.get("KAHLI_APP_SECRET", "change_me")
SESSION_COOKIE_NAME = "kahli"


# Flask-SQLAlchemy settings
# -------------------------
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "KAHLI_DATABASE_URI",
    "postgresql://postgres@postgres/postgres",
)


# Flask-Security-Too settings
# ---------------------------
SECURITY_PASSWORD_SALT = os.environ.get("KAHLI_SECURITY_PASSWORD_SALT", "foo")
SECURITY_TOTP_ISSUER = "kah·li"
SECURITY_TOTP_SECRETS = {
    1: os.environ.get(
        "KAHLI_SECURITY_TOTP_SECRETS",
        "SanFrancisco49ersFiveTimeSuperBowlChampions",
    ),
}


# Flask-Mailman settings (used by Flask-Security-Too)
# ---------------------------------------------------
# See https://waynerv.github.io/flask-mailman/#configuration for a list
# of available configuration settings, like ``MAIL_USERNAME`` or
# ``MAIL_PASSWORD``.
MAIL_SERVER = "mailhog"
MAIL_PORT = 1025
MAIL_DEFAULT_SENDER = ("kah·li", "noreply@kah.li")


# Sentry settings
# ---------------
#: A valid DSN for enabling Sentry integration.  If omitted, Sentry
#: integration will be disabled.
# SENTRY_DSN = "<protocol>://<key>@<host>/<project>"  # noqa: ERA001
SENTRY_ENVIRONMENT = "development"
SENTRY_RELEASE = importlib.metadata.version("kahli")
SENTRY_TRACES_SAMPLE_RATE = 0.0


# Application-internal configuration
# ==================================
#
# Consider all settings in this section to be “private”, i.e., they
# should never be changed.


# Flask settings
# --------------
SESSION_COOKIE_SAMESITE = "strict"


# Flask-SQLAlchemy settings
# -------------------------
# Set the ``sqlalchemy.engine`` logger’s log level to ``INFO`` or
# ``DEBUG`` if you want to see the executed SQL statements.
SQLALCHEMY_ECHO = False
SQLALCHEMY_ENGINE_OPTIONS = {
    "pool_pre_ping": True,
}
SQLALCHEMY_TRACK_MODIFICATIONS = False


# Flask-Security-Too settings
# ---------------------------
# Make endpoints available under a special path.
SECURITY_URL_PREFIX = "/kahli"
# Require users to confirm their email address when registering.
SECURITY_CONFIRMABLE = True
# Enable the user registration endpoint.
SECURITY_REGISTERABLE = True
# Enable the password reset/recover endpoint.
SECURITY_RECOVERABLE = True
# Enable the change password endpoint.
SECURITY_CHANGEABLE = True
# Enable tracking of basic user login statistics.
SECURITY_TRACKABLE = True
# Enable two-factor login.
SECURITY_TWO_FACTOR = True
SECURITY_TWO_FACTOR_ENABLED_METHODS = ["authenticator", "email"]
# Enable recovery codes for 2FA
SECURITY_MULTI_FACTOR_RECOVERY_CODES = True


# Flask-Login settings (used by Flask-Security-Too)
# -------------------------------------------------
REMEMBER_COOKIE_SAMESITE = "strict"
