"""The kah·li CLI.

You can use this module’s :func:`main` function as console script entry
point in your ``setup.py``, for example:

.. code-block:: python

   from setuptools import setup

   setup(
       ...,
       entry_points={
           'console_scripts': [
               'kahli = kahli.cli:main'
           ]
       }
   )

This allows you to run commands simply via:

.. code-block:: console
   $ kahli ...
"""

import click
from flask import Flask
from flask.cli import FlaskGroup, ScriptInfo

from .app import create_app


def _create_app_instance(_script_info: ScriptInfo) -> Flask:
    """Create an application instance."""
    return create_app()


@click.group(cls=FlaskGroup, create_app=_create_app_instance)
def main() -> None:
    """Run Flask-CLI."""
