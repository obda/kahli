"""Tests for the ``app`` module."""

import os
from unittest.mock import patch

import py.path
import pytest
import sentry_sdk
from _pytest.capture import CaptureFixture
from _pytest.monkeypatch import MonkeyPatch

from ..app import create_app
from ..config import SECRET_KEY, SESSION_COOKIE_NAME

config_envvar = "KAHLI_CONFIG"


def assert_empty_stderr(capsys: CaptureFixture[str]) -> None:
    """Assert that standard error contains nothing."""
    stdout, stderr = capsys.readouterr()
    assert stderr == ""


def test_create_app(capsys: CaptureFixture[str]) -> None:
    """Test app creation without parameters.

    The application should echo a warning and run with default settings.
    """
    app = create_app()
    stdout, stderr = capsys.readouterr()
    assert stderr == "No configuration given, using defaults\n"
    assert app.config["SECRET_KEY"] == SECRET_KEY
    assert app.config["SESSION_COOKIE_NAME"] == SESSION_COOKIE_NAME


def test_create_app_with_conffile(
    capsys: CaptureFixture[str],
    tmpdir: py.path.local,
) -> None:
    """Test app creation with a config file.

    The application should not echo a warning, and the settings from the
    given config file should override the default settings.
    """
    # noinspection PyTypeChecker
    conffile = tmpdir.join("myconfig.py")
    conf_secret_key = "youcannotguessme"  # noqa: S105
    conffile.write(f'SECRET_KEY = "{conf_secret_key}"')
    app = create_app(str(conffile))
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == conf_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == SESSION_COOKIE_NAME


def test_create_app_with_non_existing_conffile(
    capsys: CaptureFixture[str],
) -> None:
    """Test app creation with a non-existing config file.

    Application creation should fail, and an error message should be
    printed.
    """
    conffile = "/this/path/does/not/exist"
    with pytest.raises(SystemExit):
        create_app(conffile)
    stdout, stderr = capsys.readouterr()
    assert stderr == f"Cannot read config file: {conffile}\n"


def test_create_app_with_invalid_conffile(
    capsys: CaptureFixture[str],
    tmpdir: py.path.local,
) -> None:
    """Test app creation with an invalid config file.

    Application creation should fail, and an error message should be
    printed.
    """
    # noinspection PyTypeChecker
    conffile = tmpdir.join("myconfig.py")
    conffile.write('SECRET_KEY = "missingclosingquote')
    with pytest.raises(Exception):  # noqa: B017, PT011
        create_app(str(conffile))
    stdout, stderr = capsys.readouterr()
    assert stderr == f"Cannot load config file: {conffile}\n"


def test_create_app_with_environment_variable(
    capsys: CaptureFixture[str],
    monkeypatch: MonkeyPatch,
    tmpdir: py.path.local,
) -> None:
    """Test app creation with an environment variable.

    The application should not echo a warning, and the default settings
    should be overriden from the config file that is defined in the
    ``KAHLI_CONFIG`` environment variable.
    """
    # noinspection PyTypeChecker
    envfile = tmpdir.join("myconfig.py")
    env_secret_key = "youllneverguessme"  # noqa: S105
    envfile.write(f'SECRET_KEY = "{env_secret_key}"')
    monkeypatch.setitem(os.environ, config_envvar, str(envfile))
    app = create_app()
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == env_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == SESSION_COOKIE_NAME


def test_create_app_with_kwargs(capsys: CaptureFixture[str]) -> None:
    """Test app creation with kwargs.

    No warning should be echoed, and the keyword argument settings
    should override the default settings.
    """
    kwargs_secret_key = "donteventryguessingme"  # noqa: S105
    app = create_app(SECRET_KEY=kwargs_secret_key)
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == kwargs_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == SESSION_COOKIE_NAME


def test_create_app_with_conffile_and_envvar(
    capsys: CaptureFixture[str],
    monkeypatch: MonkeyPatch,
    tmpdir: py.path.local,
) -> None:
    """Test app creation with config file and environment variable.

    No warning should be echoed, and the settings from the config file
    should override the default settings.  The settings from the file
    provided via the environment variable should be ignored.
    """
    # noinspection PyTypeChecker
    conffile = tmpdir.join("myconfig-conf.py")
    conf_secret_key = "guessingisoverrated"  # noqa: S105
    conffile.write(f'SECRET_KEY = "{conf_secret_key}"')
    # noinspection PyTypeChecker
    envfile = tmpdir.join("myconfig-env.py")
    envfile.write('SECRET_KEY = "guesswhat"')
    monkeypatch.setitem(os.environ, config_envvar, str(envfile))
    app = create_app(str(conffile))
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == conf_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == SESSION_COOKIE_NAME


def test_create_app_with_conffile_and_kwargs(
    capsys: CaptureFixture[str],
    tmpdir: py.path.local,
) -> None:
    """Test app creation with config file and keyword arguments.

    No warning should be echoed, and the settings from the config file
    should override the default settings.  Finally, the settings from
    the keyword arguments should override again.
    """
    # noinspection PyTypeChecker
    conffile = tmpdir.join("myconfig.py")
    conf_cookie_name = "enoznaf"
    conffile.write(
        'SECRET_KEY = "doomedtoguess"\n'
        f'SESSION_COOKIE_NAME = "{conf_cookie_name}"',
    )
    kwargs_secret_key = "makeaneducatedguess"  # noqa: S105
    app = create_app(str(conffile), SECRET_KEY=kwargs_secret_key)
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == kwargs_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == conf_cookie_name


def test_create_app_with_envvar_and_kwargs(
    capsys: CaptureFixture[str],
    monkeypatch: MonkeyPatch,
    tmpdir: py.path.local,
) -> None:
    """Test app creation with config file and keyword arguments.

    No warning should be echoed, and the settings from the file provided
    via the environment variable should override the default settings.
    Finally, the settings from the keyword arguments should override
    again.
    """
    # noinspection PyTypeChecker
    envfile = tmpdir.join("myconfig.py")
    env_cookie_name = "enoznaf"
    envfile.write(
        'SECRET_KEY = "reallyhardtoguess"\n'
        f'SESSION_COOKIE_NAME = "{env_cookie_name}"',
    )
    monkeypatch.setitem(os.environ, config_envvar, str(envfile))
    kwargs_secret_key = "guessthis"  # noqa: S105
    app = create_app(SECRET_KEY=kwargs_secret_key)
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == kwargs_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == env_cookie_name


def test_create_app_with_conffile_and_envvar_and_kwargs(
    capsys: CaptureFixture[str],
    monkeypatch: MonkeyPatch,
    tmpdir: py.path.local,
) -> None:
    """Test app creation with config file, environment variable, and kwargs.

    No warning should be echoed, and the settings from the config file
    should override the default settings.  Finally, the settings from
    the keyword arguments should override again.  The settings from the
    file provided via the environment variable should be ignored.
    """
    # noinspection PyTypeChecker
    conffile = tmpdir.join("myconfig-conf.py")
    conf_cookie_name = "guesssomuch"
    conffile.write(
        'SECRET_KEY = "guesssomuch"\n'
        f'SESSION_COOKIE_NAME = "{conf_cookie_name}"',
    )
    # noinspection PyTypeChecker
    envfile = tmpdir.join("myconfig-env.py")
    envfile.write(
        'SECRET_KEY = "dontguesssomuch"\nSESSION_COOKIE_NAME = "nevermind"',
    )
    monkeypatch.setitem(os.environ, config_envvar, str(envfile))
    kwargs_secret_key = "youllnevergessitright"  # noqa: S105
    app = create_app(str(conffile), SECRET_KEY=kwargs_secret_key)
    assert_empty_stderr(capsys)
    assert app.config["SECRET_KEY"] == kwargs_secret_key
    assert app.config["SESSION_COOKIE_NAME"] == conf_cookie_name


def test_create_app_with_sentry_initialization() -> None:
    """Test Sentry initialization during app creation."""
    dsn = "https://key@sentry.example.com/4711"
    with patch.object(sentry_sdk, "init") as init_mock:
        create_app(SENTRY_DSN=dsn)
    init_mock.assert_called_once()
    call_kwargs = init_mock.call_args[-1]
    assert call_kwargs["dsn"] == dsn


def test_create_app_without_sentry_initialization() -> None:
    """Verify that Sentry is not initialized by default."""
    with patch.object(sentry_sdk, "init") as init_mock:
        create_app()
    init_mock.assert_not_called()


def test_create_app_sqlalchemy_echo_default() -> None:
    """Test that ``SQLALCHEMY_ECHO`` is not set by default."""
    app = create_app()
    assert app.config["SQLALCHEMY_ECHO"] is False


def test_create_app_sqlalchemy_echo_debug_mode() -> None:
    """Test that ``SQLALCHEMY_ECHO`` is not set by default in debug mode."""
    app = create_app(DEBUG=True)
    assert app.config["SQLALCHEMY_ECHO"] is False


def test_create_app_sqlalchemy_echo_explicit_set() -> None:
    """Test that ``SQLALCHEMY_ECHO`` is active if explicitly set."""
    app = create_app(SQLALCHEMY_ECHO=True)
    assert app.config["SQLALCHEMY_ECHO"] is True
