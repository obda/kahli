FROM python:3.11.5

# Create application directory, and copy local sources into it
RUN mkdir /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

# Install Python dependencies (through Poetry)
ENV POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VERSION=1.6.1 \
    POETRY_HOME=/opt/poetry
RUN curl -sSL https://install.python-poetry.org | python - \
    && ln -s /opt/poetry/bin/poetry /usr/local/bin/poetry \
    && mkdir -p /opt/poetry/cache \
    && poetry install \
    && ln -s /usr/src/app/.pdbhistory /.pdbhistory

# Expose the development web server’s port
EXPOSE 5000

# By default, run the Flask development web server
CMD flask run --host 0.0.0.0
