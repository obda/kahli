"""A simple visit to the kah·li."""

from .base import FunctionalTest


class HomePageTest(FunctionalTest):
    """Tests for the home page."""

    def test_project_title_in_home_page_source(self) -> None:
        """Verify that the home page source contains the project title."""
        self.browser.get(self.get_server_url())
        project_title = "kah·li"
        assert project_title in self.browser.page_source
