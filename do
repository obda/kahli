#!/usr/bin/env python3
"""A simple command line toolbox for frequently used commands."""

import functools
import os
import subprocess
import sys
import textwrap
from collections.abc import Callable
from pathlib import Path
from typing import ParamSpec, TypeVar

tasks = {}


def task(func: Callable) -> Callable:
    """Register a function as a task."""
    tasks[func.__name__.rstrip("_")] = func
    # Return the original function unchanged
    return func


LauncherParam = ParamSpec("LauncherParam")
LauncherRetType = TypeVar("LauncherRetType")
OriginalLauncher = Callable[LauncherParam, LauncherRetType]
DecoratedLauncher = Callable[LauncherParam, None]


def launcher(
    service: str | None = None,
) -> Callable[[OriginalLauncher], DecoratedLauncher]:
    """Launch a command, potentially inside a Docker container.

    .. note::
       This replaces the current process!
    """

    def decorator(func: OriginalLauncher) -> DecoratedLauncher:
        @task
        @functools.wraps(func)
        def wrapper(*args: str) -> None:
            command = func(*args)
            if service:
                command = ["docker-compose", "run", "--rm", service, *command]
            os.chdir(Path(__file__).parent)
            default_docker_user = f"{os.getuid()}:{os.getgid()}"
            os.environ.setdefault("DOCKER_USER", default_docker_user)
            os.execvp(command[0], command)  # noqa: S606

        return wrapper

    return decorator


@task
def help_(*_args: str) -> None:
    """Print a help message."""
    script_name = Path(__file__).name
    message = f"""\
        Usage: {script_name} <command> [<args>...]

        If you enter a command that is not listed below, that command and all
        its arguments will be executed inside a new ``web`` Docker container.

        Commands:
    """
    print(textwrap.dedent(message))  # noqa: T201
    for command in sorted(tasks):
        description = tasks[command].__doc__.splitlines()[0]
        max_command_length = 8
        if len(command) <= max_command_length:
            output = f"  {command:<8}  {description}"
        else:
            output = f"  {command}\n            {description}"
        print(output)  # noqa: T201


@launcher()
def up(*args: str) -> list[str]:
    """Start all Dockerized services."""
    return ["docker-compose", "up", *list(args)]


@launcher()
def stop(*args: str) -> list[str]:
    """Stop all Dockerized services."""
    return ["docker-compose", "stop", *list(args)]


@launcher()
def down(*args: str) -> list[str]:
    """Stop and remove Dockerized resources."""
    return ["docker-compose", "down", *list(args)]


@launcher()
def logs(*args: str) -> list[str]:
    """Show the logs of one or more Docker container(s).

    Without arguments, show and follow the ``web`` container’s logs.
    Otherwise, all passed arguments will be appended to ``docker-compose
    logs``.
    """
    if not args:
        args = ["-f", "web"]
    return ["docker-compose", "logs", *list(args)]


@launcher()
def attach() -> list[str]:
    """Attach to the running service inside the ``web`` Docker container."""
    result = subprocess.run(
        ["docker-compose", "ps", "-q", "web"],  # noqa: S603, S607
        capture_output=True,
        check=True,
        encoding="utf-8",
    )
    container_id = result.stdout.strip()
    print(  # noqa: T201
        'Attaching to service "web".  Press Ctrl-P Ctrl-Q to exit.',
    )
    return ["docker", "attach", container_id]


@launcher("web")
def test(*args: str) -> list[str]:
    """Run the test suite."""
    args = list(args)
    # Use the IPython debugger by default
    if "--pdb" in args and not any(
        arg for arg in args if arg.startswith("--pdbcls")
    ):
        args.insert(
            args.index("--pdb") + 1,
            "--pdbcls=IPython.terminal.debugger:TerminalPdb",
        )
    return ["pytest", *list(args)]


@launcher()
def poetry(*args: str) -> list[str]:
    """Run a ``poetry`` command inside the running ``web`` container."""
    # TODO(yggi49): if the web container does not run, start one!
    return [
        "docker-compose",
        "exec",
        "-u",
        "root:root",
        "web",
        "poetry",
        *list(args),
    ]


@task
def build() -> None:
    """Create an application build and a requirements file."""
    commands = (
        ["poetry", "build", "-f", "wheel"],
        [
            "poetry",
            "export",
            "--without-hashes",
            "-o",
            "dist/requirements.txt",
        ],
    )
    for command in commands:
        subprocess.run(
            [  # noqa: S603, S607
                "docker-compose",
                "run",
                "--rm",
                "web",
                *command,
            ],
            check=True,
            encoding="utf-8",
        )


@launcher("postgres")
def psql(*args: str) -> list[str]:
    """Run the PostgreSQL command line interface."""
    return ["psql", "-h", "postgres", "-U", "postgres", *list(args)]


@launcher("postgres")
def pg_dump(*args: str) -> list[str]:
    """Run the ``pg_dump`` command line utility."""
    return ["pg_dump", "-h", "postgres", "-U", "postgres", *list(args)]


def execute_from_commandline(*argv: str) -> None:
    """Execute the task with the given command and arguments."""
    try:
        command = argv[1]
    except IndexError:
        # If no command was given, run `help` by default
        command = "help"
    try:
        my_task = tasks[command]
    except KeyError:
        # If task was not found, run the command inside a “web” Container
        my_task = launcher("web")(lambda *args: [command, *list(args)])
    my_task(*argv[2:])


if __name__ == "__main__":
    execute_from_commandline(*sys.argv)
